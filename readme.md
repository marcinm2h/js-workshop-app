# Zadania

Katalog /src/ zawiera zadania:
  - JavaScript
  - jQuery
  - AJAX

Zadania znajdują się w komentarzach do kodu.

W katalogach answer/ znajdują się odpowiedzi do nich.

Jeśli utkniesz z którymś podejrzyj część odpowiedzi.

# Używanie debuggera Chrome w Node
- chrome://inspect/#devices
- node --inspect-brk index.js