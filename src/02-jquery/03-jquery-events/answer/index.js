'use strict';

jQuery(function($) {
  const App = {
    // 1. Do obiektu App, dodaj metodę onButtonClick()
    //    w której wywołasz drugą metodę this.destroy();
    onButtonClick: function() {
      this.destroy();
    },

    // 2. Dodaj metodę this.destroy() w któtrej wywołasz alert('destroy')
    destroy: function() {
      alert('destroy');
    },

    init: function() {
      const $todoList = $('.todo-list');
      const $listElement = $(`
        <li>
          <div class="view">
            <input class="toggle" type="checkbox" />
            <label>Element listy</label>
            <button class="destroy"></button>
          </div>
        </li>
      `);

      $todoList.html($listElement);
      // 2. Dodaj event listener do przycisku button.destroy
      //    w którym wywołasz onButtonClick()
      //
      //    - pamiętaj o 'bindowaniu' eventu tak aby
      //      callback został wywołany z poprawną wartośćią 'this'
      //      (this powinien wskazywać na App)
      //
      //    - pamiętaj, że przycisk .destroy nie istnieje w momencie
      //      dodawania callbacka - musisz użyć selektora do elementu todoList
      //      i przefiltrować elementy $todoList.on(event, selektor-filr, callback)
      //      (event delegation - delegacja zdarzeń)
      $todoList.on('click', 'button.destroy', this.onButtonClick.bind(this));
    },
  };

  App.init();
});
