// 1. Zadeklaruj strict mode na początku pliku ('use strict';)
'use strict';

// 2. Wywołaj kod na document ready używając jQuery
//    (http://learn.jquery.com/using-jquery-core/document-ready/)
jQuery(function($) {
  // 3. Wewnątrz $(document).ready utwórz obiekt-moduł z metodą init
  //    App.init()
  //    (https://learn.jquery.com/code-organization/concepts/#the-object-literal)
  const App = {
    init: function() {
      console.log('App.init()');
    }
  };
  App.init();
});
