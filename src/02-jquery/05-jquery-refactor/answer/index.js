'use strict';

jQuery(function($) {
  const App = {
    init: function() {
      // 1. Wynieś kod zwiazany z renderowaniem do metody this.render()
      // -- render --
      // -- render --

      // 2. Wynieś podpinanie eventów do metody this.bindEvents() 

      // 3. Wywołaj metody render() i bindEvents()

      // 4. Upewnij się, że metoda 'init' jest na samej górze obiektu
      //    - jest pierwszym kluczem App
      // 5. Metody pomocnicze warto przenieść na dół modułu (onButtonClick, destroy)
      this.render();
      this.bindEvents();
    },

    render: function() {
      const $todoList = $('.todo-list');
      const $listElement = $(`
        <li>
          <div class="view">
            <input class="toggle" type="checkbox" />
            <label>Element listy</label>
            <button class="destroy"></button>
          </div>
        </li>
      `);
      $todoList.html($listElement);
    },

    bindEvents: function() {
      $('.todo-list').on('click', 'button.destroy', this.onButtonClick.bind(this));
    },

    onButtonClick: function(e) {
      this.destroy(e.target);
    },

    destroy: function(button) {
      $(button).closest('li').fadeOut();
    },


  };

  App.init();
});
