'use strict';

jQuery(function($) {
  const App = {
    onButtonClick: function(e) {
      this.destroy(e.target);
    },

    destroy: function(button) {
      $(button).closest('li').fadeOut();
    },

    init: function() {
      // 1. Wynieś kod zwiazany z renderowaniem do metody this.render()
      // -- render --
      const $todoList = $('.todo-list');
      const $listElement = $(`
        <li>
          <div class="view">
            <input class="toggle" type="checkbox" />
            <label>Element listy</label>
            <button class="destroy"></button>
          </div>
        </li>
      `);

      $todoList.html($listElement);
      // -- render --   

      // 2. Wynieś podpinanie eventów do metody this.bindEvents() 
      $todoList.on('click', 'button.destroy', this.onButtonClick.bind(this));

      // 3. Wywołaj metody render() i bindEvents()

      // 4. Upewnij się, że metoda 'init' jest na samej górze obiektu
      //    - jest pierwszym kluczem App
      // 5. Metody pomocnicze warto przenieść na dół modułu (onButtonClick, destroy)
   
    },
  };

  App.init();
});
