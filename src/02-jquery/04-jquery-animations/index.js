'use strict';

jQuery(function($) {
  const App = {
    // 1. Dodaj argument (event) do metody onButtonlick
    //    wywołaj metodę this.destroy() z celem - target eventu
    //    this.destroy(e.target)
    onButtonClick: function() {
      this.destroy();
    },

    // 2. Dodaj argument funkcji (button)
    destroy: function() {
      // 3. Utwórz element jQuery z buttona ($(button))
      // 4. Wyszukaj rodzica elementu button będącego elementem <li></li>
      //    - trawersowanie po drzewie DOM do góry
      // 5. Ukryj element z drzewa DOM przez metodę fadeOut();
    },

    init: function() {
      const $todoList = $('.todo-list');
      const $listElement = $(`
        <li>
          <div class="view">
            <input class="toggle" type="checkbox" />
            <label>Element listy</label>
            <button class="destroy"></button>
          </div>
        </li>
      `);

      $todoList.html($listElement);
      $todoList.on('click', 'button.destroy', this.onButtonClick.bind(this));
    },
  };

  App.init();
});
