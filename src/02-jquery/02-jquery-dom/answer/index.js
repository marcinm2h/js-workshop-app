'use strict';

jQuery(function($) {
  const App = {
    init: function() {
      // - na razie wykonaj wszystko w metodzie 'init'
      // 1. Przyjrzyj się strukturze kodu html w index.html
      // 2. Utwórz selektor do listy <ul></ul> (selektor do klasy) i
      //    zadeklaruj go jako zmienną
      const $todoList = $('.todo-list');

      // 3. Z użyciem jQuery utwórz element:
      //      <li>
      //        <div class="view">
      //          <input class="toggle" type="checkbox" />
      //          <label>Element listy</label>
      //          <button class="destroy"></button>
      //        </div>
      //      </li>
      //    - użyj tempate string (``) dla kilku linijkowych stringów
      const $listElement = $(`
        <li>
          <div class="view">
            <input class="toggle" type="checkbox" />
            <label>Element listy</label>
            <button class="destroy"></button>
          </div>
        </li>
      `);

      // 4. Używając jQuery ustaw zawartoś listy (zadeklarowanej w punkcie 2)
      //    jako stworzony w punkcie 3 element
      $todoList.html($listElement);
    },
  };

  App.init();
});
