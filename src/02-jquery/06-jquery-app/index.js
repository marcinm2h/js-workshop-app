'use strict'; // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
const uuid = window.uuid.v4;

const ENTER_KEY = 13;

// 1. Aplikacja została rozszerzona o dodatkowe metody
//    - zapoznaj się z działaniem i poeksperymentuj z kodem
// 2. Do elementu listy dodaj datepicker z jQueryUI
//    - pamiętaj o zaimportowaniu biblioteki w index.html
//      oraz stylów css
// 3. Po wybraniu daty - jeśli data jest starsza niż data aktualna
//    ustaw stan todo jako completed i wywołaj ponowny render()
//    - możesz porównać sformatowaną datę leksygraficznie
//      string dd/mm/yy do stringa
//    - $.datepicker.formatDate('dd/mm/yy', new Date()); - data dzisiejsza
//    - pamiętaj o formacie daty - dateFormat
jQuery(function($) {
  const App = {
    init: function() {
      this.todos = [];
      this.bindEvents();
    },

    render: function() {
      const todosHtml = this.todos.map(todo => this.todoTemplate(todo));
      $('.todo-list').html(todosHtml);
    },

    todoTemplate: function(todo) {
      return `
        <li class="${todo.completed ? 'completed' : ''}" data-id="${todo.id}">
          <div class="view">
            <input class="toggle" type="checkbox" ${todo.completed ? 'checked' : ''}>
            <label>${todo.title}</label>
            <button class="destroy"></button>
          </div>
        </li>`;
    },

    bindEvents: function() {
      // wywołanie funkcji callbacka zmienia 'this' na element.target
      // dlatego trzeba zbindować event - przypisać mu wartość 'this' na stałę
      // lub użyć arrow function() => {}
      $('.new-todo').on('keyup', e => {
        if (e.which === ENTER_KEY) {
          this.create(e);
        }
      });

      // delegacja zdarzeń (event delegation)
      // callbacki podpinane są na istniejący element .todo-list
      // i filtrowane po elemencie, który był ich targetem

      // selektory bezpośrednie ($('.todo-list .toggle')) w tym miejscu
      // nie działałyby, ponieważ te elementy ('.toggle', '.destroy') w momencie
      // wywołania bindEvents() jeszcze nie isniteją
      $('.todo-list')
        .on('change', '.toggle', this.toggle.bind(this))
        .on('click', '.destroy', this.destroy.bind(this));
    },

    getTodoFromElement: function(el) {
      const id = $(el).closest('li').data('id');
      return this.todos.find((todo = {}) => todo.id === id);
    },

    create: function(e) {
      const $input = $(e.target);
      const val = $input.val().trim();
      if (!val) {
        return;
      }
      this.todos.push({
        id: uuid(),
        title: val,
        completed: false
      });
      $input.val('');
      this.render();
    },

    toggle: function(e) {
      const todo = this.getTodoFromElement(e.target);
      todo.completed = !todo.completed;
      this.render();
    },

    update: function(e) {
      const el = e.target;
      const val = $(el).val();
      if (!val) {
        return;
      }
      const todo = this.getTodoFromElement(el);
      todo.title = val;
      this.render();
    },

    destroy: function(e) {
      const $listElement = $(e.target).closest('li');
      $listElement.fadeOut({
        duration: 100,
        done: () => {
          const todo = this.getTodoFromElement(e.target);
          this.todos = this.todos.filter(t => t !== todo);
          this.render();
        } 
      });
    }
  };

  App.init();
});
