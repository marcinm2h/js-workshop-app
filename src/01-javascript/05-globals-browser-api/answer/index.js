// - encodeURI / encodeURIComponent
// - decodeURI / decodeURIComponent 
// - isFinite / isNaN
// - parseInt / parseFloat
// - Number / String / Array / Object / RegExp / Symbol
// - eval
(function(){
  'use strict';
  // 1. Zparsuj wracające z serwera liczby typu float(parseFloat, Number)
  //               0      1      2       3      4      5       6
  const numbers = ['1.1', '1.0a', '1.1', '1$', null, undefined, ''];
  const parsed = [
    Number(numbers[0]),
    parseFloat(numbers[1]),
    parseFloat(numbers[2]),
    parseFloat(numbers[3]),
    parseFloat(numbers[4]),
    parseFloat(numbers[5]),
    parseFloat(numbers[6])
  ];

  // 2. Dla sparsowanych danych typu NaN wypisz w konsoli (console.error('błąd parsowania'))
  //    a dla poprawnych wypisz tę liczbę pomnożoną 2 (musi być to liczba wymierna)
  parsed.forEch(function(number, idx) {
    if (isNaN(number)) {
      console.error('błąd parsowania', numbers[idx]);
    } else if (isFinite(number)) {
      console.log(number * 5);
    }
  }); 

})();
