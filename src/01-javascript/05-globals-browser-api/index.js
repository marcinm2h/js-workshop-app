// - encodeURI / encodeURIComponent
// - decodeURI / decodeURIComponent 
// - isFinite / isNaN
// - parseInt / parseFloat
// - Number / String / Array / Object / RegExp / Symbol
// - eval

(function(){
  'use strict';
  // 1. Zparsuj wracające z serwera liczby typu float(parseFloat, Number)
  //               0      1      2       3      4      5       6
  const numbers = ['1.1', '1.0a', '1.1', '1$', null, undefined, ''];
  const parsed = [
    Number(numbers[0]),
    numbers[1],
    numbers[2],
    numbers[3],
    numbers[4],
    numbers[5],
    numbers[6]
  ];

  // 2. Dla sparsowanych danych typu NaN wypisz w konsoli (console.error('błąd parsowania'))
  //    a dla poprawnych wypisz tę liczbę pomnożoną 2 (musi być to liczba wymierna)

})();
