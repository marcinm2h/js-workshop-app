const mock = require('./mock.json');

const api = {
  getUsers: () => mock.users,
  getConfig: () => mock.config
};

// 1. Pobierz z api listę użytkowników (api.getUsers())
//    i wypisz ją w konsoli
//    (console.log, console.info, console.error, console.table, ...)
const users = api.getUsers();

// 2. Dla każdego użytkownika wypisz jego imię (name)
//    i dodaj je do nowej tablicy userNames

// 3. Utwórz tablicę użytkowników będącymi mężczyznami (user.gender === 'male')

// 4. Znajdź najcięższego usera.
//    user.mass
//    Pamiętaj, że api zwraca wartości jako stringi nie Number

// 5. Napisz funkcję szukającą użytkowników wyższych niż jej argument
//    Pamiętaj, że api zwraca wartości jako stringi, a funkcja na wejściu
//    ma dostawać wartości typu Number

// 6. Z konfiguracji (getConfig()) odczytaj (jeśli istnieją) pola
//    config.users.name.maxLength
//    config.page.protocol.type
//    Api zapewnia istnienie jedynie obiektu config
//    każdy następny klucz (config.a, config.b, ...)
//    może nie istnieć

// 7. Napisz "klasę" User.
//    Klasa będzie posiadać pola "name", "gender", "mass"
//    ustawiane w konstruktorze (argumencie funkcji)
//    oraz metodę: 'info' zwracającą string informacje o obiekcie
function User() {}
const user01 = new User();

// 8. Poprzez domnknięcie (closure) stwórz funkcję-fabrykę która
//    zwróci funkcję-licznik. Każde kolejne wywołanie funkcji zwiększa licznik o 1.
//    const counter = getCounter();
//    let val = counter(); // 0
//    val = counter(); // 1
