const mock = require('../mock.json');

const api = {
  getUsers: () => mock.users,
  getConfig: () => mock.config
};

// 1. Pobierz z api listę użytkowników (api.getUsers())
//    i wypisz ją w konsoli
//    (console.log, console.info, console.error, console.table, ...)
const users = api.getUsers();
// console.log(users.data);

// 2. Dla każdego użytkownika wypisz jego imię (name)
//    i dodaj je do nowej tablicy userNames
let userNames = [];
// ES6
for (let user of users.data) {
  console.log(user.name);
  userNames.push(user.name);
}
// ES5
for (let idx in users.data) {
  let user = users.data[idx];
  console.log(user.name);
  userNames.push(user.name);
}
// Styl funcyjny - deklaratywny
userNames = users.data.map(user => user.name);
console.log(userNames);

// 3. Utwórz tablicę użytkowników będącymi mężczyznami (user.gender === 'male')
const males = [];
for (let user of users.data) {
  // console.log(user.name);
  if (user.gender === 'male') {
    males.push(user.name);
  }
}
console.log('males', males);

// 4. Znajdź najcięższego usera.
//    user.mass
//    Pamiętaj, że api zwraca wartości jako stringi nie Number
let heaviest;
for (let user of users.data) {
  if ((heaviest === undefined) || (Number(user.mass) > Number(heaviest.mass))) {
    heaviest = user;
  }
}
console.log('heaviest', heaviest);

// 5. Napisz funkcję szukającą użytkowników wyższych niż jej argument
//    Pamiętaj, że api zwraca wartości jako stringi, a funkcja na wejściu
//    ma dostawać wartości typu Number
function filterHeavierThan(limit, users) {
  let usersHeavierThan = [];
  for (let user of users) {
    if (Number(user.mass) > limit) {
      usersHeavierThan.push(user);
    }
  }

  return usersHeavierThan;
}

const heavyUsers = filterHeavierThan(100, users.data);
console.log('heavyUsers', heavyUsers);

// 6. Z konfiguracji (getConfig()) odczytaj (jeśli istnieją) pola
//    config.users.name.maxLength
//    config.page.protocol.type
//    Api zapewnia istnienie jedynie obiektu config
//    każdy następny klucz (config.a, config.b, ...)
//    może nie istnieć
const config = api.getConfig();
const userNameMaxLength = config && config.users && config.users.name && config.users.name.maxLength || 100;
const pageProtocolType = config && config.page && config.page.protocol && config.page.protocol.type || 'http';
console.log('userNameMaxLength', userNameMaxLength, 'pageProtocolType', pageProtocolType);

// 7. Napisz "klasę" User.
//    Klasa będzie posiadać pola "name", "gender", "mass"
//    ustawiane w konstruktorze (argu encie funkcji)
//    oraz metodę: 'info' zwracającą string informacje o obiekcie
function User({ name, gender, mass }) {
  this.name = name;
  this.gender = gender;
  this.mass = mass;
}
User.prototype.info = function() {
  return `User {name=${this.name}, gender=${this.gender}, mass=${this.mass}}`;
};

const user01 = new User({ name: 'login01', gender: 'male', mass: '99.0' });
console.log(user01.info());

// 8. Poprzez domnknięcie (closure) stwórz funkcję-fabrykę króra
//    zwróci funkcję-licznik. Każde kolejne wywołanie funkcji zwiększa licznik o 1.
//    const counter = getCounter();
//    let val = counter(); // 0
//    val = counter(); // 1
function getCounter(defaultValue = 0) {
  let value = defaultValue;
  return function inc() {
    return value++;
  };
}

const counter = getCounter();
let val = counter(); // 0
console.log(0, 'val', val);
val = counter(); // 1
console.log(1, 'val', val);
