// Zadanie do uruchamiania w przeglądarce
// z obsługą modułów es6 (np. chrome >= 61)

// 1. Utwórz pliki a.js, b.js i c.js
// 2. W pliku b.js eksportuj domyślnie funkcję:
//    function b() { console.log('b.js')/ }
// 3. W pliku c.js utwórz funkcję:
//    function c() { console.log('c.js' ); }
//    i eksportuj ją jako nazwany export (export { c })
// 3. W pliku a.js importuj funkcje z pliku b.js
//    i c.js i wywołaj obie
// 4. W pliku index.js importuj cały moduł a.js
//    (import './a.js';)
