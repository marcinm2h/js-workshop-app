// Zadanie do wykonania w środowisku NodeJS.
// Użyj modułów commonJS. Do importowania użyj require('./plik.js')
// eksportuj przez globalny obiekt module.exports

// 1. Utwórz pliki a.js, b.js i c.js
// 2. W pliku b.js eksportuj domyślnie funkcję:
//    function b() { console.log('b.js')/ }
// 3. W pliku c.js utwórz funkcję:
//    function c() { console.log('c.js' ); }
//    i eksportuj ją jako module.exports.c
// 3. W pliku a.js importuj funkcje z pliku b.js
//    i c.js i wywołaj obie
// 4. W pliku index.js importuj cały moduł a.js
require('./a.js');
