// alert, prompt, confirm

(function() {
  'use strict';
  // 1. Po wejściu na stronę wyswietl komunikat:
  // Cześć! Policzę twój rok urodzenia.
  alert('Cześć! Policzę twój rok urodzenia.');

  // 2. Następnie w 3 kolejnych
  //    dialogach poproś użytkownika o podanie
  //    1) roku, 2) miesiąca 3) dnia
  //    urodzin i następnie zapisz te dane do 3ch zmiennych
  let y = prompt('podaj rok urodzin');   
  let m = prompt('podaj miesiąc urodzin');   
  let d = prompt('podaj dzień urodzin');

  // 3. Zapytaj użytkownika czy podał poprawna datę urodzin
  //    Jeśli nie - wyświetl komunikat:
  //    Odśwież stronę aby spróbować ponownie
  //    i zakończ działanie programu (zakończ funkcje-moduł przez 'return');
  const isCorrect = confirm(`Czy podana data - ${y}-${m}-${d} - jest poprawna?`);
  if (!isCorrect) {
    alert('Odśwież stronę aby spróbować ponownie');
    return;
  }

  // 4. Użyj funkcji diffInYears() do policzenia różnicy w latach i wyświetl
  //    wartość użytkownikowi
  function diffInYears(y, m, d) {
    const timestamp = Date.parse(`${y}-${m}-${d}`);
    return Math.floor((Date.now() - timestamp) / 31536000000);
  }
  const age = diffInYears(y, m, d);
  alert(`Masz ${age} lat!`);
})();


