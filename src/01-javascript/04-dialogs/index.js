// alert, prompt, confirm

(function() {
  'use strict';
  // 1. Po wejściu na stronę wyswietl komunikat:
  // Cześć! Policzę twój rok urodzenia.

  // 2. Następnie w 3 kolejnych
  //    dialogach poproś użytkownika o podanie
  //    1) roku, 2) miesiąca 3) dnia
  //    urodzin i następnie zapisz te dane do 3ch zmiennych

  // 3. Zapytaj użytkownika czy podał poprawna datę urodzin
  //    Jeśli nie - wyświetl komunikat:
  //    Odśwież stronę aby spróbować ponownie
  //    i zakończ działanie programu (zakończ funkcje-moduł przez 'return');

  // 4. Użyj funkcji diffInYears() do policzenia różnicy w latach i wyświetl
  //    wartość użytkownikowi
})();


