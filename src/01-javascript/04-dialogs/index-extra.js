// * w index.html zmień ścieżkę do skryptu na index-extra.js *
(function() {
  'use strict';
  alert('Cześć! Policzę twój rok urodzenia.');

  //   Wynieś poniższy kod do funkcji, tak
  //    aby w momencie kiedy użytkownik
  //    poda niepoprawną datę otrzymał komunikat:
  //    Spróbuj ponownie
  //    i funckja została ponownie wywołana.
  let y = prompt('podaj rok urodzin');   
  let m = prompt('podaj miesiąc urodzin');   
  let d = prompt('podaj dzień urodzin');

  const isCorrect = confirm(`Czy podana data - ${y}-${m}-${d} - jest poprawna?`);
  if (!isCorrect) {
    alert('Odśwież stronę aby spróbować ponownie');
    return;
  }

  function diffInYears(y, m, d) {
    const timestamp = Date.parse(`${y}-${m}-${d}`);
    return Math.floor((Date.now() - timestamp) / 31536000000);
  }
  const age = diffInYears(y, m, d);
  alert(`Masz ${age} lat!`);
})();
